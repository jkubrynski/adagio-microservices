package com.adagio.api;

import com.adagio.payment.api.PaymentStatus;

public class PaymentNotification {

	private final String id;
	private final PaymentStatus status;

	public PaymentNotification(String id, PaymentStatus status) {
		this.id = id;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public PaymentStatus getStatus() {
		return status;
	}
}
