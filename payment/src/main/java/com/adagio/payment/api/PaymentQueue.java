package com.adagio.payment.api;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PaymentQueue {

	@Output("payments")
	MessageChannel channel();
}
