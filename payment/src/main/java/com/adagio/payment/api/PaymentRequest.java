package com.adagio.payment.api;

import java.math.BigDecimal;

@SuppressWarnings("unused")
class PaymentRequest {

	private BigDecimal value;

	public BigDecimal getValue() {
		return value;
	}
}
