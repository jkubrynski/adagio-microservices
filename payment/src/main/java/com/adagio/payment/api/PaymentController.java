package com.adagio.payment.api;

import java.lang.invoke.MethodHandles;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@RequestMapping("/payments")
class PaymentController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentService paymentService;

	PaymentController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<PaymentDetails> registerPayment(@RequestBody PaymentRequest paymentRequest) {
		LOG.info("Received payment registration request: {}", paymentRequest.getValue());
		PaymentDetails paymentDetails = paymentService.registerPayment(paymentRequest);
		URI location = MvcUriComponentsBuilder
				.fromMethodCall(on(PaymentController.class).getPaymentDetails(paymentDetails.getId()))
				.build()
				.toUri();

		return ResponseEntity.created(location).body(paymentDetails);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	PaymentDetails getPaymentDetails(@PathVariable String id) {
		return paymentService.getPaymentDetails(id);
	}

	@PutMapping(value = "/{id}")
	void receivePayment(@PathVariable String id) {
		LOG.info("Reecived payment: {}", id);
		paymentService.paymentReceived(id);
	}
}
