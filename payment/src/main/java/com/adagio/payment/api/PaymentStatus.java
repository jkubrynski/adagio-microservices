package com.adagio.payment.api;

public enum PaymentStatus {
	NEW, RECEIVED, PAID
}
