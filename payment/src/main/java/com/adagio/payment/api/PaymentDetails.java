package com.adagio.payment.api;

import java.math.BigDecimal;

class PaymentDetails {

	private final String id;
	private final BigDecimal amount;
	private final PaymentStatus status;

	PaymentDetails(String id, BigDecimal amount, PaymentStatus status) {
		this.id = id;
		this.amount = amount;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public PaymentStatus getPaymentStatus() {
		return status;
	}
}
