package com.adagio.payment.api;

import java.awt.*;
import java.lang.invoke.MethodHandles;
import java.math.BigDecimal;
import java.security.AlgorithmConstraints;
import java.util.HashMap;
import java.util.UUID;

import com.adagio.api.PaymentNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
class PaymentService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentQueue paymentQueue;

	PaymentService(PaymentQueue paymentQueue) {
		this.paymentQueue = paymentQueue;
	}

	PaymentDetails registerPayment(PaymentRequest paymentRequest) {
		LOG.info("Registering payment");
		return new PaymentDetails(UUID.randomUUID().toString(), paymentRequest.getValue(), PaymentStatus.NEW);
	}

	PaymentDetails getPaymentDetails(String id) {
		return new PaymentDetails(id, BigDecimal.TEN, PaymentStatus.PAID);
	}

	void paymentReceived(String id) {
		LOG.info("Processing payment notification: {}", id);
		HashMap<String, Object> headers = new HashMap<>();
		headers.put("contentType", "application/json");
		paymentQueue.channel().send(MessageBuilder.createMessage(new PaymentNotification(id, PaymentStatus.RECEIVED), new MessageHeaders(headers)));
	}
}
