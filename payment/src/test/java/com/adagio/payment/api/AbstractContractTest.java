package com.adagio.payment.api;

import java.math.BigDecimal;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMessageVerifier
public class AbstractContractTest {

	@Autowired
	PaymentService paymentService;

	@Before
	public void setUp() throws Exception {
		PaymentService paymentService = Mockito.mock(PaymentService.class);
		Mockito.when(paymentService.registerPayment(any(PaymentRequest.class)))
				.thenReturn(new PaymentDetails("2", BigDecimal.TEN, PaymentStatus.NEW));
		RestAssuredMockMvc.standaloneSetup(new PaymentController(paymentService));
	}

	void triggerPaymentReceivedMessage() {
		paymentService.paymentReceived("12345");
	}
}
