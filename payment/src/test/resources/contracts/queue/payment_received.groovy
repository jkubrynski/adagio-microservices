package contracts.queue

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	label 'paymentReceived'
	input {
		triggeredBy('triggerPaymentReceivedMessage()')
	}
	outputMessage {
		sentTo('payments')
		body('{"id":"12345", "status":"RECEIVED"}')
		headers {
			messagingContentType(applicationJson())
		}
	}
}