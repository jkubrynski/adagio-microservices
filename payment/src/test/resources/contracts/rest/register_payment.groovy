package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		url '/payments'
		method 'POST'
		headers {
			contentType(applicationJson())
		}
		body(
				'value': 10
		)
	}
	response {
		status 201
		headers {
			header(location(), 'http://localhost/payments/2')
			contentType(applicationJson())
		}
		body(
				"id" : "2",
				"amount": 10,
				"status" : "NEW"
		)
	}
}