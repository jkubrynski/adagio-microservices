package com.adagio.auction.api;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;

import feign.Response;
import org.codehaus.plexus.util.IOUtil;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentClientTest extends AbstractPaymentContractTest {

	@Autowired
	PaymentClient paymentClient;

	@Test
	public void shouldRegisterPayment() throws IOException {
		ResponseEntity<PaymentDetails> response = paymentClient.registerPayment(new PaymentRequest(BigDecimal.TEN));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(response.getHeaders().getLocation()).isEqualTo(URI.create("http://localhost/payments/2"));
		assertThat(response.getBody().getId()).isEqualToIgnoringCase("2");
	}
}