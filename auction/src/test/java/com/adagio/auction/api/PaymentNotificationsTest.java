package com.adagio.auction.api;

import com.adagio.api.PaymentNotification;
import com.adagio.api.PaymentStatus;
import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.contract.stubrunner.StubFinder;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentNotificationsTest extends AbstractPaymentContractTest {

	@Autowired
	StubFinder stubFinder;

	@Autowired
	PaymentNotificationsListener paymentNotificationsListener;

	@Test
	public void shouldReceivePaymentNotification() {
		// when
		stubFinder.trigger("paymentReceived");

		// then
		PaymentNotification recentMessage = paymentNotificationsListener.getRecentMessage();
		assertThat(recentMessage).isNotNull();
		assertThat(recentMessage.getId()).isEqualTo("12345");
		assertThat(recentMessage.getStatus()).isEqualTo(PaymentStatus.RECEIVED);
	}
}
