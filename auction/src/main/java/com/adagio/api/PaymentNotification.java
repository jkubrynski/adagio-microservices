package com.adagio.api;

public class PaymentNotification {
	private String id;
	private PaymentStatus status;

	public String getId() {
		return id;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "PaymentNotification{" + "id='" + id + '\'' +
				", status=" + status +
				'}';
	}
}
