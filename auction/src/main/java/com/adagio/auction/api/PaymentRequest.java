package com.adagio.auction.api;

import java.math.BigDecimal;

class PaymentRequest {

	private BigDecimal value;

	public PaymentRequest(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}
}
