package com.adagio.auction.api;

import java.lang.invoke.MethodHandles;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auctions")
class AuctionController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final AuctionService auctionService;

	AuctionController(AuctionService auctionService) {
		this.auctionService = auctionService;
	}

	@PutMapping("/{auctionId}/finish")
	URI payForAuction(@PathVariable String auctionId) {
		LOG.info("Finishing auction: {}", auctionId);
		return auctionService.payForAuction(auctionId);
	}

}
