package com.adagio.auction.api;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface PaymentsQueue {

	String QUEUE_NAME = "payments";

	@Input(QUEUE_NAME)
	SubscribableChannel channel();

}
