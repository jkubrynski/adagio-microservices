package com.adagio.auction.api;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import feign.Response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PaymentClientFallback implements PaymentClient {

	@Override
	public ResponseEntity<PaymentDetails> registerPayment(PaymentRequest paymentRequest) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(URI.create("http://other-service.com/payments/1234"));
		return new ResponseEntity<>(httpHeaders, HttpStatus.CREATED);
	}
}
