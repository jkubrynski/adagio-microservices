package com.adagio.auction.api;

import java.lang.invoke.MethodHandles;
import java.math.BigDecimal;
import java.net.URI;

import com.adagio.auction.data.AuctionDetails;
import com.adagio.auction.data.AuctionRepository;
import feign.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.util.locale.provider.LocaleServiceProviderPool;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class AuctionService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentClient paymentClient;
	private final AuctionRepository auctionRepository;

	AuctionService(PaymentClient paymentClient, AuctionRepository auctionRepository) {
		this.paymentClient = paymentClient;
		this.auctionRepository = auctionRepository;
	}

	public URI payForAuction(String auctionId) {
		LOG.info("Creatinig payment request: {}", auctionId);
		AuctionDetails auction = auctionRepository.findAuction(auctionId);
		ResponseEntity response = paymentClient.registerPayment(new PaymentRequest(auction.getValue()));
		return response.getHeaders().getLocation();
	}
}
