package com.adagio.auction.api;

import java.lang.invoke.MethodHandles;

import com.adagio.api.PaymentNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class PaymentNotificationsListener {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private PaymentNotification recentMessage;

	@StreamListener(PaymentsQueue.QUEUE_NAME)
	void handleMessage(PaymentNotification notification) {
		LOG.info("Received payment notification: {}", notification);
		recentMessage = notification;
	}

	public PaymentNotification getRecentMessage() {
		return recentMessage;
	}
}
