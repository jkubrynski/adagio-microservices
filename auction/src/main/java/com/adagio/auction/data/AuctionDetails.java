package com.adagio.auction.data;

import java.math.BigDecimal;

public class AuctionDetails {

	private final BigDecimal value;

	AuctionDetails(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return value;
	}
}
